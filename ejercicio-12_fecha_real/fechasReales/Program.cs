﻿using System;

namespace fechasReales
{
    class Program
    {
        static void Main(string[] args)
        {
            pedirFechaRegistro();
        }

        static void pedirFechaRegistro()
        {
            DateTime fechaRango;
            bool correcto = false;

            Console.WriteLine("Ingresa una Fecha en formato dd/mm/aaaa");
            string fecha = Console.ReadLine();

            while (!correcto)  //Mientras no sea correcto...
            {
                if (!DateTime.TryParse(fecha, out fechaRango))
                {
                    Console.WriteLine("La fecha no es válida, ingrese de nuevo");
                    fecha = Console.ReadLine();
                }
                else
                {
                    correcto = true;
                }
            }

            Console.WriteLine("La fecha {0} es correcta", fecha);
           
        }
    }
}
