﻿using System;

namespace MesesDelAño
{
    class Program
    {
        static void Main(string[] args)
        {
            int mes;
            Boolean salir = false;
            
            while (salir==false)
            {
                Console.WriteLine("Ingresa un numero del 0 al 11");
                mes = int.Parse(Console.ReadLine());
                switch (mes)
                {
                    case 0:
                        Console.WriteLine("Enero");
                        break;
                    case 1:
                        Console.WriteLine("Febrero");
                        break;
                    case 2:
                        Console.WriteLine("Marzo");
                        break;
                    case 3:
                        Console.WriteLine("Abril");
                        break;
                    case 4:
                        Console.WriteLine("Mayo");
                        break;
                    case 5:
                        Console.WriteLine("Junio");
                        break;
                    case 6:
                        Console.WriteLine("Julio");
                        break;
                    case 7:
                        Console.WriteLine("Agosto");
                        break;
                    case 8:
                        Console.WriteLine("Septiembre");
                        break;
                    case 9:
                        Console.WriteLine("Octubre");
                        break;
                    case 10:
                        Console.WriteLine("Noviembre");
                        break;
                    case 11:
                        Console.WriteLine("Diciembre");
                        break;
                    default:
                        Console.WriteLine("numero invalido , Ingresa un numero entre 0 y 11");
                        break;
                }
            }
         


        }
    }
}
/*Ejercicio 11. Meses del año - (opcional)
Problema: Elabora un programa que reciba un número entero entre 0 y 11, debe devolver el nombre de mes correspondiente.
Toma en cuenta que 0 = Enero y 11 = Diciembre.

Crea un subdirectorio en tu repositorio con el nombre "ejercicio-11_meses"
En ese directorio deberás entregar los siguientes elementos*/