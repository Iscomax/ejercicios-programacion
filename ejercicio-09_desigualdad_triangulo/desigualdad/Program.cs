﻿using System;

namespace desigualdad
{
    class Program
    {
        static void Main(string[] args)
        {
            double ladoA=0, ladoB=0, ladoC=0;
            double sumaAB, sumaAC, sumaBC;
            Boolean igualdad = false;
            Console.WriteLine("Ingresa el primer lado del triangulo");
            ladoA = double.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa el segundo lado del triangulo");
            ladoB = double.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa el tercer lado del triangulo");
            ladoC = double.Parse(Console.ReadLine());
            
            sumaAB = ladoA + ladoB;
            
            if (sumaAB>ladoC)
            {
                sumaAC = ladoA + ladoC;
                if (sumaAC>ladoB)
                {
                    sumaBC = ladoB + ladoC;
                    if (sumaBC>ladoA)
                    {
                        igualdad = true;
                    }
                }

            }
            else
            {
                igualdad = false;
            }



            if (igualdad==true)
            {
                Console.WriteLine("La igualdad se cumple");
            }
            else
            {
                Console.WriteLine("La igualdad no se cumple");
            }

        }
    }
}
//ejecutar en visual studio  abrir el archivo "Tablas_Multiplicar.sln"
//menu --> depurar-->iniciar depuracion o presionar f5
//para ver el codido menu-->ver --> explorador de soluciones 
//para ver el codido menu-->ver --> vista de clases 
