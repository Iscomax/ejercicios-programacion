﻿using System;
//ejecutar en rextester.com--- o en visual studio creando un nuevo proyecto
namespace ejercicios
{
    class Program
    {
        static void Main(string[] args)
        {
            double lado = 0;
            double perimetro = 0;
            Console.WriteLine("Este programa calcula el área de un triangulo equilátero");
            Console.Write("Ingresa el valor del lado: ");
            //lado =15;
            lado = double.Parse(Console.ReadLine());//comentar en rextester
            perimetro = lado * 3;
            Console.WriteLine("El perimetro del triangulo = " + perimetro);
        }
    }
}
