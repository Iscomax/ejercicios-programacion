﻿using System;

namespace Volados
{
    class Program
    {
        static void Main(string[] args)
        {

            int pidio;
            int saldoUsuario =500, saldoCPU =500;
            int apuesta = 0;
            String cayo = "";
            int rondas = 0;
            while (rondas !=3)
            {
                Console.WriteLine("\n Bienvenido a los volados");
                Console.WriteLine("\n El saldo de la CPU para apostar es de: " + saldoCPU);
                
                Console.WriteLine("\n Tu saldo para apostar es de: " +saldoUsuario );
                Console.WriteLine("El rango de apuesta es de  $20 a $100");
                Console.Write(" Teclea la cantidad que deseas apostar : ");
                apuesta = int.Parse(Console.ReadLine());
                Console.WriteLine("\n Que pides: 1.- águila o 2.-Sol?");
                pidio = int.Parse(Console.ReadLine());

                if (apuesta>=20 && apuesta<=100)
                {
                    Random random1 = new Random();
                    int volado = random1.Next(1, 3);
                    if (pidio == volado)
                    {
                        if (volado == 1)
                        {
                            cayo = "Águila";

                        }
                        else
                        {
                            cayo = "Sol";
                        }

                        Console.WriteLine("felicidades ganaste");
                        Console.WriteLine(" Cayó =" + cayo);
                        saldoUsuario = saldoUsuario + apuesta;
                        saldoCPU = saldoCPU - apuesta;
                        Console.WriteLine("saldo = " + saldoUsuario);
                        Console.WriteLine("saldo CPU = " + saldoCPU);
                        rondas++;
                    }
                    else
                    {
                        if (volado == 1)
                        {
                            cayo = "Águila";

                        }
                        else
                        {
                            cayo = "Sol";

                        }
                        Console.WriteLine("Perdiste");
                        Console.WriteLine("Cayó =" + cayo);
                        saldoUsuario = saldoUsuario - apuesta;
                        saldoCPU = saldoCPU + apuesta;
                        Console.WriteLine("saldo = " + saldoUsuario);
                        Console.WriteLine("saldo CPU = " + saldoCPU);
                        rondas++;
                    }
                }
                else
                {
                    Console.WriteLine("Apuesta fuera del rango, el rango es de  $20 a $100");
                }

                

            }
            
            if (saldoUsuario>= saldoCPU)
            {
                Console.WriteLine("Despues de 3 rondas el ganador eres tu");
            }
            else
            {
                Console.WriteLine("Despues de 3 rondas el ganador es la CPU");
            }

        }
    }
}
