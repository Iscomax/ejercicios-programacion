﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;


//ejecutar en visual studio  abrir el archivo "codeKata.sln"
//menu --> depurar-->iniciar depuracion o presionar f5
//para ver el codido menu-->ver --> explorador de soluciones 
//para ver el codido menu-->ver --> vista de clases 


namespace codeKata
{
    class Program
    {
        static void Main(string[] args)
        {
            int A, R, A2, R2;
            int resultado;
            string dupla = "";
            Boolean salir = false;
            while (salir==false)
            {
                Console.WriteLine("");
                Console.WriteLine("Kata : Números arábigos y números romanos");
                Console.WriteLine("La entrada del programa es una cadena formada por dos pares 'AR' ");
                Console.WriteLine("ejemplo: '4X3C' o '3C4X' ");
                Console.Write("Ingresa un par : ");

                dupla = Console.ReadLine();
                char[] arreglo = dupla.ToUpper().ToCharArray(); ;//alamcenamos la cadena en un arreglo char


                A = (int)Char.GetNumericValue(arreglo[0]);//Conversion de char a Int

                Arabigo arabigo = new Arabigo();
                R = arabigo.conversion(arreglo[1]);

                A2 = (int)Char.GetNumericValue(arreglo[2]);


                R2 = arabigo.conversion(arreglo[3]);


                if (R >= R2)
                {
                    resultado = (A * R) + (A2 * R2);

                    Console.WriteLine("El Resultado es :" + resultado);
                }
                else
                {
                    resultado = -(A * R) + (A2 * R2);

                    Console.WriteLine("El Resultado es :" + resultado);
                }

            }




        }
    }
}
