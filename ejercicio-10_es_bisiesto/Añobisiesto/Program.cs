﻿using System;

namespace Añobisiesto
{
    class Program
    {
        static void Main(string[] args)
        {
            int año = 0;

            Console.WriteLine("Ingresa el año");
            año = int.Parse(Console.ReadLine());

            AñoBisiesto añoBisiesto = new AñoBisiesto();
            añoBisiesto.calcularAñoBisiesto(año);


        }

        class AñoBisiesto
        {
            public AñoBisiesto()
            {
            }

            public void calcularAñoBisiesto(int año)
            {
                if ((año % 4 == 0) && ((año % 100 != 0) || (año % 400 == 0)))
                {
                    Console.WriteLine("El año es bisiesto");
                }
                else
                {
                    Console.WriteLine("El año no es bisiesto");
                }
            }
        }
    }
}
