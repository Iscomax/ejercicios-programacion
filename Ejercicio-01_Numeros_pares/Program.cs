﻿using System;
using System.Reflection;

//ejecutar en https://rextester.com/ --- o en visual studio creando un nuevo proyecto

namespace Ejercicio_01_Numeros_pares
{
    class Program
    {
        static void Main(string[] args)
        {
            int contador = 0;
             while (contador <=100)
             {
                 if ( contador %2 ==0)
                 {
                     Console.WriteLine("el  numero " + contador + " es par ");
                 }
                 contador++;
             }
            
        }
    }
}
