﻿using System;
//ejecutar en rextester.com--- o en visual studio creando un nuevo proyecto
namespace ejercicios
{
    class Program
    {
        static void Main(string[] args)
        {
            double ladoA = 0;
            double ladoB = 0;
            double ladoC = 0;
            double perimetro = 0;
            Boolean salir = false;
            Console.WriteLine("Programa que calcula el perimetro de un  triángulo escaleno ");
            Console.WriteLine("Recuerda que El perímetro de un triángulo escaleno es la suma de sus tres lados, ya que éstos tres son diferentes.");
                while (salir ==false)
                {
                Console.WriteLine("Ingresa el valor del primer lado ");
                // ladoA=0;
                ladoA = double.Parse(Console.ReadLine());//comentar en rextester
                Console.WriteLine("Ingresa el valor del segundo lado ");
                // ladobB=0;  
                ladoB = double.Parse(Console.ReadLine());//comentar en rextester
                Console.WriteLine("Ingresa el valor del tercer lado ");
                // ladoC=0;  
                ladoC = double.Parse(Console.ReadLine());//comentar en rextester

                    if ((ladoA != ladoB) && (ladoB != ladoC) && (ladoA != ladoC))
                            {
                                perimetro = ladoA + ladoB + ladoC;
                                Console.WriteLine("El perimetro del triangulo es de:" +perimetro);
                                salir = true;
                            }
                            else
                            {
                                Console.WriteLine("Los tres lados del triangulo tienen que ser diferentes ");
                            }
                }

        }
    }
}
