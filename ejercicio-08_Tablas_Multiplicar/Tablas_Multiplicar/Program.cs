﻿using System;

namespace Tablas_Multiplicar
{
    class Program
    {
        static void Main(string[] args)
        {

            int tabla;
            Boolean salir = false;
            while (salir==false)
            {
                Console.WriteLine("Ingresa el numero de la tabla de multiplicar");
                tabla = int.Parse(Console.ReadLine());
                Console.WriteLine("La tabla del " + tabla + " es:");
                for (int i = 2; i <= 10; i++)
                {
                    Console.WriteLine(tabla + " x " + i + " = " + tabla * i);
                }
            }
        }
    }
}
