using System;
//ejecutar en rextester.com  --- o en visual studio creando un nuevo proyecto
namespace ejercicios
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero = 0;
            int suma = 0;
            int contador = 1;

            Console.WriteLine("ingresa un numero entre el 1 y el 50 ");
            //numero =  12;
            numero = int.Parse(Console.ReadLine());// en rextester comentar int.Parse(Console.ReadLine())
            if (numero <= 50)
            {
                while (contador <= numero)
                {
                    suma = suma + contador;
                    contador++;
                }
                Console.WriteLine("la suma del 1 al " + numero + " es de : " + suma);

            }
            else
            {
                Console.WriteLine("el numero es mayor a 50");
            }
        }
    }
}
