﻿using System;
//ejecutar en rextester.com--- o en visual studio creando un nuevo proyecto
namespace ejercicios
{
    class Program
    {
        static void Main(string[] args)
        {
            double pesos = 0;
            double precioDolar = 22.36;
            double dolares = 0;

            Console.WriteLine("Ingresa la cantidad de dolares a convertir a pesos ");
            //dolares = 25;
            dolares = double.Parse(Console.ReadLine());//comentar en rextester

            pesos = dolares * precioDolar;

            Console.WriteLine(dolares + " Dolares Americanos son equivalentes a " + pesos + " mexicanos");
        }
    }
}
