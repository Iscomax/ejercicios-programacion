﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Perimetro_triangulo
{
    class Perimetro
    {//definimos los atributos de la clase
        double ladoA, ladoB, ladoC;
        double perimetro;
        double resultado;
        public Perimetro()
        {

        }
        //metodos que calculan los perimetros de cada triangulo
       public double equilateroPerimetro( )
        {

            perimetro = ladoA * 3;
           
           return perimetro;
        }

       public double isoscelesPerimetro()
        {
            perimetro = (2 * ladoA) + ladoB;

            return perimetro;
        }
      
        public double escalenoPerimetro()
        {

            if ((ladoA != ladoB) && (ladoB != ladoC) && (ladoA != ladoC))
            {
                perimetro = ladoA + ladoB + ladoC;
                //Console.WriteLine("El perimetro del triangulo es de:" + perimetro);
               
            }
            else
            {
                Console.WriteLine("Los tres lados del triangulo tienen que ser diferentes ");
            }
            return perimetro;
        }
        //metodos que piden los dalos de cada triangulo ---------------------------

        public void pedirValorEquilatero()
        {
            Console.WriteLine("El triángulo equilátero es aquel que se caracteriza por tener todos los lados iguales");
            Console.Write("\n");
            Console.Write("Ingresa el valor del lado: ");

            ladoA = double.Parse(Console.ReadLine());
           
        }
        //-------------------------------------------------------------------
        public void pedirValorIsosceles()
        {
            Console.Write("\n");
            Console.WriteLine("Calcular Triangulo isósceles ");
            Console.Write("\n");
            Console.WriteLine("Recuerda que El triángulo isósceles es un polígono de tres lados, siendo dos iguales y el otro desigual.");
            Console.Write("\n");
            Console.Write("Ingresa el valor de los lados Iguales: ");
            ladoA = double.Parse(Console.ReadLine());
            Console.Write("\n");
            Console.Write("Ingresa el valor del tercer lado: ");
            ladoB = double.Parse(Console.ReadLine());
        }
        public void pedirValoresEscalenos()
        {
            Console.Write("\n");
            Console.WriteLine("Calcular Triangulo escaleno ");
            Console.Write("\n");
            Console.WriteLine("Recuerda que El perímetro de un triángulo escaleno es la suma de sus tres lados, ya que éstos tres son diferentes.");
            Console.Write("\n");
            Console.Write("Ingresa el valor del primer lado: ");
      
            ladoA = double.Parse(Console.ReadLine());
            Console.Write("Ingresa el valor del segundo lado: ");
  
            ladoB = double.Parse(Console.ReadLine());
            Console.Write("Ingresa el valor del tercer lado: ");
       
            ladoC = double.Parse(Console.ReadLine());
        }

        //metodo que muestra el valor de la variable perimetro 
        public void mostrarPerimetro()
        {
            Console.WriteLine("El perimetro del triangulo es =" + perimetro);
        }
    }
}
