﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Perimetro_triangulo
{
    class Menu
    {
        public Menu()
        {

        }

        public void mostrar()
        {
            Boolean salir = false;
            int opcion = 0;

            while (salir == false)
            {
                Console.Write("\n");
                Console.WriteLine("Programa que calcula el perimetro de un triangulo Ingresa una opcion del menu");
                Console.Write("\n");
                Console.WriteLine("\n 1.-Triangulo equilátero\n 2.-Triangulo isósceles\n 3.-Triangulo escaleno\n 4.-Salir\n");
              
                opcion = int.Parse(Console.ReadLine());

                switch (opcion)
                {
                    case 1:

                        Perimetro equilatero = new Perimetro();
                        equilatero.pedirValorEquilatero();
                        equilatero.equilateroPerimetro();
                        equilatero.mostrarPerimetro();
                        break;
                    case 2:
                        Perimetro isosceles = new Perimetro();
                        isosceles.pedirValorIsosceles();
                        isosceles.isoscelesPerimetro();
                        isosceles.mostrarPerimetro();

                        break;
                    case 3:
                        Perimetro escaleno = new Perimetro();
                        escaleno.pedirValoresEscalenos();
                        escaleno.escalenoPerimetro();
                        escaleno.mostrarPerimetro();
                        break;
                    case 4:
                        salir = true;
                        break;
                    default:
                        Console.WriteLine("opcion invalida, porfavor ingresa una opcion del menu ");
                        break;
                }
            }
        }
    }
}
