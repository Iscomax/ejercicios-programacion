﻿using System;
//ejecutar en rextester.com--- o en visual studio creando un nuevo proyecto
namespace ejercicios
{
    class Program
    {
        static void Main(string[] args)
        {
            double ladoA = 0;
            double ladoB = 0;
            double perimetro = 0;
            Console.WriteLine("Programa que calcula el perimetro de un triangulo isósceles ");
            Console.WriteLine("Recuerda que El triángulo isósceles es un polígono de tres lados, siendo dos iguales y el otro desigual.");
            Console.WriteLine("Ingresa el valor de los lados iguales ");
            //ladoA=0;
            ladoA = double.Parse(Console.ReadLine());//comentar en rextester
            Console.WriteLine("Ingresa el valor del tercer lado ");
            //ladoB=0;
            ladoB = double.Parse(Console.ReadLine());//comentar en rextester
            perimetro = (2 * ladoA) + ladoB;
            Console.WriteLine("El perimetro del triangulo es de: " +perimetro);

        }
    }
}
